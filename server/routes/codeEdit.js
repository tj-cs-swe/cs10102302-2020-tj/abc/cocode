"use strict";

const express = require("express");
const router = express.Router();
const cm = require("../cocodeManage");
const { EvtBus } = require("../utils");
//const { Project } = require("../db/db");

// 计算用户房间的角色：
//  没有编程 === 1；
//  开始编程（房间拥有者）=== 2；
//  开始编程（普通人员）=== 3；
function getUserRoomRole(hasJoinRoom, roomOwner) {
    if (!hasJoinRoom) {
        return 1;
    }

    return roomOwner ? 2 : 3;
}

// 进入结对编程界面，有三种可能：
// 1、没有编程               === 1
// 2、开始编程（房间拥有者）   === 2
// 3、开始编程（普通人员）     === 3
// 操作可能：
// 1、自己改变文档；2、退出编程；3、被人邀请
// 改变文档：
//    对于可能1，就是简单的新建room，成为creator
//    对于可能2，
//        如果room里还有其他人，提醒用户，通知room里的其他人，指定其他人的第一个人为creator，离开room，新开room，成为creator
//        如果room里没有其他人，改变房间数据（项目名称，文件名），继续留在房间做creator
//    对于可能3，
//        如果room里还有其他人，提醒用户，通知room里的其他人，离开room，新开room，成为creator
//        如果room里没有其他人，改变房间数据（项目名称，文件名），继续留在房间做creator
// 退出编程：
//    对于可能2，
//        如果room里还有其他人，提醒用户，通知room里的其他人，指定其他人的第一个人为creator，离开room
//        如果room里没有其他人，直接退出room，销毁room数据
//    对于可能3，
//        如果room里还有其他人，提醒用户，通知room里的其他人，离开room
//        如果room里没有其他人，直接退出room，销毁room数据
// 被人邀请：
//    对于可能1，就是简单的同意和拒绝，同意就加入
//    对于可能2，
//        如果room里还有其他人，提醒用户，通知room里的其他人，指定其他人的第一个人为creator，离开room，进入邀请人room，成为普通人员
//        如果room里没有其他人，销毁room数据，离开room，进入邀请人room，成为普通人员
//    对于可能3，
//        如果room里还有其他人，提醒用户，通知room里的其他人，离开room，进入邀请人room，成为普通人员
//        如果room里没有其他人，销毁room数据，离开room，进入邀请人room，成为普通人员

router.post("/", async(req, res) => {
    const operation = req.body.operation;
    // console.log(req.body);
    // const findProject = await Project.findOne({
    //     ProjectName: req.body.projectName,
    // });
    // if (!findProject) {
    //     res.send({
    //         status: "Not_Exist",
    //     });
    //     return;
    // }

    // 改变文档类动作
    if (operation === "CreateRoom") {
        const projectName = req.body.projectName;
        const filePath = req.body.filePath;
        const roomId = req.body.roomId;
        const userName = req.body.userName;
        // const nickName = req.body.nickName;
        // const avatar = req.body.avatar;
        // const color = req.body.color;
        const roomOwner = req.body.roomOwner;
        const hasJoinRoom = req.body.hasJoinRoom;

        const userRoomRole = getUserRoomRole(hasJoinRoom, roomOwner);

        // console.log(req.body);
        const room = cm.getRoom(roomId);

        // 没有编程情况
        if (userRoomRole === 1 || room === null || room === undefined) {
            // 判断是否已经在某个房间里
            const hasRoom = cm.findRoom(projectName, filePath, userName);
            if (!hasRoom) {
                // 判断是否有其他人编辑文件
                const sameFileEditRoomId = cm.hasSameEditFile(projectName, filePath);
                if (sameFileEditRoomId > 0) {
                    // 发回讯提醒前端是否加入其它的room共同编辑
                    const sameFileRoom = cm.getRoom(sameFileEditRoomId);
                    res.send({
                        status: 2, // 加入别人的房间
                        leaveNotify: false,
                        creatorChangeNotify: false, // 房屋拥有者改变提醒
                        editorChangeNotify: false, // 编辑者改变提醒
                        newUser: "",
                        data: {
                            roomId: sameFileEditRoomId,
                            roomOwner: false,
                            fileContent: sameFileRoom ? sameFileRoom.fileContent : "",
                        },
                    });
                } else {
                    const newRoomId = cm.createRoom(projectName, filePath, userName);
                    res.send({
                        status: 1, // 正常开房间
                        leaveNotify: false,
                        creatorChangeNotify: false, // 房屋拥有者改变提醒
                        editorChangeNotify: false, // 编辑者改变提醒
                        newUser: "",
                        data: {
                            roomId: newRoomId,
                            roomOwner: true,
                        },
                    });
                }
            } else {
                res.send({
                    status: 1, // 正常开房间
                    leaveNotify: false,
                    creatorChangeNotify: false, // 房屋拥有者改变提醒
                    editorChangeNotify: false, // 编辑者改变提醒
                    newUser: "",
                    data: {
                        roomId: hasRoom.roomId,
                        roomOwner: hasRoom.creator === userName,
                    },
                });
            }

            return;
        }

        // 已编程（房间拥有者或者普通人员）
        //const room = cm.getRoom(roomId);

        // 判断房间里是否有其他人
        if (!room.hasOtherUsers(userName)) {
            // 判断是否有人在编辑同一个文件
            const sameFileEditRoomId = cm.hasSameEditFile(projectName, filePath);
            if (sameFileEditRoomId > 0) {
                // 发回讯提醒前端是否加入其它的room共同编辑
                const sameFileRoom = cm.getRoom(sameFileEditRoomId);
                res.send({
                    status: 2, // 加入别人的房间
                    leaveNotify: false,
                    creatorChangeNotify: false, // 房屋拥有者改变提醒
                    editorChangeNotify: false, // 编辑者改变提醒
                    newUser: "",
                    needReback: true,
                    originRoomId: roomId,
                    data: {
                        roomId: sameFileEditRoomId,
                        roomOwner: false,
                        fileContent: sameFileRoom ? sameFileRoom.fileContent : "",
                    },
                });

                return;
            }

            // 没有其他人编辑同一个文件，直接把房间的数据更换，
            cm.updateRoomInfo(roomId, projectName, filePath);
            res.send({
                status: 1, // 正常开房间
                leaveNotify: false,
                creatorChangeNotify: false, // 房屋拥有者改变提醒
                editorChangeNotify: false, // 编辑者改变提醒
                newUser: "",
                data: {
                    roomId: roomId,
                    roomOwner: true,
                },
            });
            return;
        }

        // 如果有则换房间，同时通知其他人离开，
        // 房间creator和普通客户的区别在于，要移交room所有权，其它一样
        const firstOtherUser = room ? room.firstOtherUser(userName) : "";

        let creatorChangeNotify = false;
        if (userRoomRole === 2) {
            // 置其他人的第一个为房间建立者
            room.creator = firstOtherUser;
            creatorChangeNotify = true;
        }

        // 如果当前编辑人是自己，简单粗暴的置为null
        let editorChangeNotify = false;
        if (room.editor === userName) {
            room.editor = firstOtherUser;
            editorChangeNotify = true;
        }

        // 把自己从房间中删除
        room.removeUser(userName);

        // 判断换房间的逻辑：
        // 是否有其他人在编辑新的文件，如果有，则加入，并通知其他房间的人
        // 判断是否有其他人编辑文件
        const sameFileEditRoomId = cm.hasSameEditFile(projectName, filePath);
        if (sameFileEditRoomId > 0) {
            // 发回讯提醒前端是否加入其它的room共同编辑
            const sameFileRoom = cm.getRoom(sameFileEditRoomId);
            res.send({
                status: 2, // 加入别人的房间
                leaveNotify: true, // 通知原房间的人离开
                originRoomId: roomId,
                creatorChangeNotify: creatorChangeNotify, // 房屋拥有者改变提醒
                editorChangeNotify: editorChangeNotify, // 编辑者改变提醒
                newUser: firstOtherUser,
                data: {
                    roomId: sameFileEditRoomId,
                    roomOwner: false,
                    fileContent: sameFileRoom ? sameFileRoom.fileContent : "",
                },
            });

            return;
        }

        // 没有人编辑新文件，正常开新房间
        // const newRoomId = cm.createRoom(projectName, filePath, userName);
        // res.send({
        //     status: 1, // 正常开房间
        //     leaveNotify: true, // 通知原房间的人离开
        //     originRoomId: roomId,
        //     creatorChangeNotify: false, // 房屋拥有者改变提醒
        //     editorChangeNotify: false, // 编辑者改变提醒
        //     newUser: "",
        //     data: {
        //         roomId: newRoomId,
        //         roomOwner: true,
        //     },
        // });

        const newRoomId = cm.createRoom(projectName, filePath, userName);
        res.send({
            status: 1, // 正常开房间
            leaveNotify: true, // 通知原房间的人离开
            originRoomId: roomId,
            creatorChangeNotify: creatorChangeNotify, // 房屋拥有者改变提醒
            editorChangeNotify: editorChangeNotify, // 编辑者改变提醒
            newUser: firstOtherUser,
            data: {
                roomId: newRoomId,
                roomOwner: true,
            },
        });

        return;
    }

    // 被邀请后换房间
    if (operation === "ChangeRoom") {
        const roomId = req.body.roomId;
        const userName = req.body.userName;
        const roomOwner = req.body.roomOwner;
        const hasJoinRoom = req.body.hasJoinRoom;
        const targetRoomId = req.body.targetRoomId;

        const userRoomRole = getUserRoomRole(hasJoinRoom, roomOwner);

        // 已编程（房间拥有者或者普通人员）
        const room = cm.getRoom(roomId);
        const targetRoom = cm.getRoom(targetRoomId);

        let creatorChangeNotify = false;
        let editorChangeNotify = false;
        const firstOtherUser = room ? room.firstOtherUser(userName) : "";

        if (room) {
            // 判断房间里是否有其他人
            if (!room.hasOtherUsers(userName)) {
                // 没有直接删除room并返回
                cm.removeRoom(roomId);

                res.send({
                    status: 1, // 正常返回
                    leaveNotify: false,
                    creatorChangeNotify: creatorChangeNotify, // 房屋拥有者改变提醒
                    editorChangeNotify: editorChangeNotify, // 编辑者改变提醒
                    newUser: "",
                    fileContent: targetRoom ? targetRoom.fileContent : "",
                });
                return;
            }

            // 如果有则换房间，同时通知其他人离开，
            // 房间creator和普通客户的区别在于，要移交room所有权，其它一样
            if (userRoomRole === 2) {
                // 置其他人的第一个为房间建立者
                room.creator = firstOtherUser;
                creatorChangeNotify = true;
            }

            // 如果当前编辑人是自己，设置为第一个其他人
            if (room.editor === userName) {
                room.editor = firstOtherUser;
                editorChangeNotify = true;
            }

            // 把自己从房间中删除
            room.removeUser(userName);
        }

        res.send({
            status: 1, // 正常返回
            leaveNotify: room ? true : false, // 通知原房间的人离开
            originRoomId: roomId,
            creatorChangeNotify: creatorChangeNotify, // 房屋拥有者改变提醒
            editorChangeNotify: editorChangeNotify, // 编辑者改变提醒
            newUser: firstOtherUser,
            fileContent: targetRoom ? targetRoom.fileContent : "",
        });

        return;
    }

    // 判断用户是否已经在某个房间里，如在，则返回对应的数据
    if (operation === "IsInRoom") {
        const userName = req.body.userName;
        // const projectName = req.body.projectName;
        // const filePath = req.body.filePath;
        // const roomId = req.body.roomId;

        // 判断是否已经在某个房间里
        const room = cm.inRoom(userName);
        // console.log("InRoom? ", userName, room);

        if (!room) {
            res.send({
                status: "ERROR",
            });
        } else {
            res.send({
                status: "OK",
                data: {
                    roomId: room.roomId,
                    projectName: room.projectName,
                    file: room.file,
                    fileContent: room.fileContent,
                    creator: room.creator,
                    isEditor: room.editor === userName,
                    roomOwner: room.creator === userName,
                },
            });
        }

        return;
    }

    if (operation === "QuitRoom") {
        const userName = req.body.userName;
        const roomId = req.body.roomId;
        const roomOwner = req.body.roomOwner;
        // const hasJoinRoom = req.body.hasJoinRoom;

        const room = cm.getRoom(roomId);

        if (!room) {
            res.send({
                status: 1, // 正常结束
                creatorChangeNotify: false, // 房屋拥有者改变提醒
                editorChangeNotify: false, // 编辑者改变提醒
                newUser: "",
                data: {
                    roomId: cm.makeRoomId(),
                },
            });

            return;
        }

        // 判断房间里是否还有其它人
        const hasOtherUsers = room.hasOtherUsers(userName);

        let returnStatus;
        let creatorChangeNotify = false;
        let editorChangeNotify = false;

        const firstOtherUser = room ? room.firstOtherUser(userName) : "";

        if (hasOtherUsers) {
            // 有其它人，返回结束标示2，发起通知，告知其它人员退出了
            if (roomOwner) {
                // 房间creator
                room.creator = firstOtherUser;
                creatorChangeNotify = true;
            }

            if (room.editor === userName) {
                room.editor = firstOtherUser;
                editorChangeNotify = true;
            }

            // 删除房间里的自己
            room.removeUser(userName);

            returnStatus = 2;
        } else {
            // 没有其它人，直接删除room，返回结束标示
            cm.removeRoom(roomId);

            returnStatus = 1;
        }

        res.send({
            status: returnStatus, // 正常结束
            creatorChangeNotify: creatorChangeNotify, // 房屋拥有者改变提醒
            editorChangeNotify: editorChangeNotify, // 编辑者改变提醒
            newUser: firstOtherUser,
            data: {
                roomId: cm.makeRoomId(),
            },
        });

        return;
    }

    // 单纯的删除房间逻辑，没有回讯
    if (operation === "CloseRoom") {
        const roomId = req.body.roomId;
        cm.removeRoom(roomId);
    }

    // 当项目删除了某个user后，删除对应的room里的user
    if (operation === "DeleteRoomUser") {
        const projectName = req.body.projectName;
        const userName = req.body.userName;

        const _room = cm.inRoom(userName);

        if (_room && _room.projectName === projectName) {
            // 删除room里的人员
            _room.removeUser(userName);

            if (_room.users.length === 0) {
                cm.removeRoom(_room.roomId);
            }
        }

        // 发消息通知room里的其他人，有人退出了
        EvtBus.publish("CodeEdit.SendProjectUserRemoveMsg", {
            roomId: _room ? _room.roomId : 0,
            projectName: projectName,
            userName: userName,
            message: `${userName}已经从项目中删除，将退出编程。`,
        });
    }

    // 当删除项目后，删除项目对应的所有room
    if (operation === "DeleteRooms") {
        const projectName = req.body.projectName;
        const userName = req.body.userName;

        const _rooms = cm.findRoomsByProjectName(projectName);
        if (!_rooms || _rooms.length === 0) {
            EvtBus.publish("CodeEdit.SendProjectRemoveMsg", {
                roomId: 0,
                projectName: projectName,
                userName: userName,
                message: `项目：${projectName} 已经被删除了，退出编程。`,
            });
            return;
        }

        _rooms.forEach(_room => {
            // 发消息通知删除room里的所有人，项目取消了
            EvtBus.publish("CodeEdit.SendProjectRemoveMsg", {
                roomId: _room.roomId,
                projectName: projectName,
                userName: userName,
                message: `项目：${projectName} 已经被删除了，退出编程。`,
            });
            cm.removeRoom(_room.roomId);
        });
    }
});

module.exports = router;