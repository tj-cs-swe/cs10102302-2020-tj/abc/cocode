"use strict";

//注册(还未处理用户名已存在等问题)
const express = require("express");
const router = express.Router();
const { User } = require("../db/db");

/* register */
router.post("/", async (req, res) => {
    const findUser = await User.findOne({
        username: req.body.userName,
    });

    if (findUser) {
        console.log(findUser);
        return res.send({
            status: "USERNAME_DUPLICATE",
            message: "用户名已存在",
        });
    }

    const user = await User.create({
        username: req.body.userName,
        password: req.body.password,
        nickName: req.body.nickName ? req.body.nickName : req.body.userName,
        avatar: req.body.avatar ? req.body.avatar : "",
        color: req.body.color ? req.body.color : "",
    });
    //console.log(req.body);
    res.send({
        status: "GET_SUCCESS",
        user,
    });
});

module.exports = router;
