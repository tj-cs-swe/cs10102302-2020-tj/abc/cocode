"use strict";

const express = require("express");
const router = express.Router();
const { Project } = require("../db/db");
//const { User } = require("../db/db");

router.post("/", async(req, res) => {
    // console.log("write session test:   ", req.session, req.session.loginUser);

    // User.findOne({ username: req.body.userName }, function(err, data) {
    const condition = { Member: { $elemMatch: { name: req.body.userName } } };
    //const opt = { _id: 0, projectName: 2, projectDesc: 3, Member: 1 };
    Project.find(condition, function(err, data) {
        if (err) {
            console.log(err);
        }

        let prjs = [];
        if (data) {
            // prjs = data.Projects.map((_prj) => {
            prjs = data.map(_prj => {
                return {
                    projectId: _prj._id,
                    projectName: _prj.ProjectName,
                    projectDesc: _prj.ProjectDesc ? _prj.ProjectDesc : "",
                    owner: _prj.Owner,
                    createTime: _prj.CreatDate,
                    member: _prj.Member.map(_usr => {
                        return {
                            userName: _usr.name,
                        };
                    }),
                };
            });
        }

        res.send({
            status: "SUCCESS",
            projects: prjs,
        });
    });

    // User.findOne({ username: req.body.userName }, function(err, data) {
    //     if (err) {
    //         console.log(err);
    //     }
    // });
});

module.exports = router;
