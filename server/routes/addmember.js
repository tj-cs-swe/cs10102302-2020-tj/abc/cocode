"use strict";

const express = require("express");
const router = express.Router();
const { User, Project } = require("../db/db");

router.post("/", async(req, res) => {
    const findProject = await Project.findOne({
        ProjectName: req.body.projectName,
    });

    if (!findProject) {
        //console.log(findProject);
        return res.send({
            status: "ADDMEMBER_FAILURE",
            message: "项目不存在",
        });
    }

    const findUser = await User.findOne({
        username: req.body.userName,
    });
    //     console.log(findProject.Member);

    if (!findUser) {
        return res.send({
            status: "ADDMEMBER_FAILURE",
            message: "用户不存在",
        });
    }

    if (req.body.projectName.length === 0 || req.body.projectName.length > 30) {
        return res.send({
            status: "ADDMEMBER_FAILURE",
            message: "项目名称输入不对",
        });
    }

    if (req.body.userName.length === 0 || req.body.userName.length > 20) {
        return res.send({
            status: "ADDMEMBER_FAILURE",
            message: "用户名称输入不对",
        });
    }

    const condition = {
        $and: [
            { ProjectName: req.body.projectName },
            { Member: { $elemMatch: { name: req.body.userName } } },
        ],
    };
    const opt = { Member: 1, _id: 1, ProjectName: 1 };
    Project.findOne(condition, opt, function(err, doc) {
        if (err) {
            console.log(err);
        }

        if (doc) {
            return res.send({
                status: "ADDMEMBER_FAILURE",
                message: "用户已存在于此项目中",
            });
        }

        const con = { ProjectName: req.body.projectName };
        const oper = { $push: { Member: { name: req.body.userName } } };
        Project.updateOne(con, oper, function(err, raw) {
            if (err) {
                console.log(err);
            }
            if (raw) {
                console.log(raw);
            }
        });
        User.updateOne({ username: req.body.userName }, { $push: { Projects: { projectname: req.body.projectName } } },
            function(err, raw) {
                if (err) {
                    console.log(err);
                }
                if (raw) {
                    console.log(raw);
                }
            },
        );
        res.send({
            status: "ADDMEMBER_SUCCESS",
        });
    });
    /*
            const project = findProject.Member.push({
                name: req.body.userName
            });
            findProject.save();
            const user = findUser.Projects.push({
                projectname: req.body.projectName
            });
            findUser.save();
            res.send({
                status: 'ADDMEMBER_SUCCESS',
                project
            });*/
    /* Project.find({ 'Member.name': req.body.userName }, function (err, comment) {
         //     console.log(comment);
         if (comment.length == 0) {//没有此用户，可以插入
             const project = findProject.Member.push({
                 name: req.body.userName
             });
             findProject.save();

             res.send({
                 status: 'ADDMEMBER_SUCCESS',
                 project
             });
         }
         else {//已经有此用户了，不能插入
             //   console.log(comment);
             res.send({
                 status: 'ADDMEMBER_FAILURE',
                 message: '此项目中已包含此成员'
             });

         }
     });
    */
    console.log(req.body);
});

module.exports = router;