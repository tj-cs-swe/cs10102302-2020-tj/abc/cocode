"use strict";

const express = require("express");
const router = express.Router();
const { User, Project } = require("../db/db");
const fs = require("fs");
const settings = require("../settings");

// 递归删除文件夹
function deleteFolder(path) {
    let files = [];
    if (fs.existsSync(path)) {
        files = fs.readdirSync(path);
        files.forEach(function (file, index) {
            const curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) {
                // recurse
                deleteFolder(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}

router.post("/", async (req, res) => {
    Project.findOne({ ProjectName: req.body.projectName }, function (err, doc) {
        if (!doc) {
            res.send({
                status: "DELETE_FAILURE",
                message: "项目不存在",
            });
            return;
        }

        if (doc.Owner === req.body.userName) {
            const path = settings.baseProjectPath + "/" + req.body.projectName;
            deleteFolder(path);
            Project.deleteOne({ ProjectName: req.body.projectName }, function (err, docs) {
                if (err) {
                    console.log(err);
                } else {
                    const condition = {
                        Projects: { $elemMatch: { projectname: req.body.projectName } },
                    };
                    const operation = {
                        $pull: { Projects: { projectname: req.body.projectName } },
                    };
                    User.updateMany(condition, operation, function (err, raw) {
                        if (err) {
                            console.log(err);
                        }
                        if (raw) {
                            console.log(raw);
                        }
                    });
                    res.send({
                        status: "DELETE_SUCCESS",
                        message: "删除成功",
                    });
                }
            });
        } else {
            res.send({
                status: "DELETE_FAILURE",
                message: "非项目所有者，禁止删除",
            });
        }
    });
    console.log(req.body);
});

module.exports = router;
