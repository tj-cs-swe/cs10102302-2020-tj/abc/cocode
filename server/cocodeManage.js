"use strict";

const _ = require("lodash");
const { EvtBus } = require("./utils");

class Room {
    constructor(roomId) {
        this.roomId = roomId;
        this.projectName = "";
        this.file = "";
        this.fileContent = "";
        this.creator = null;
        this.editor = null;
        this.users = [];
    }

    addUser(userName) {
        const index = _.findIndex(this.users, _user => _user.userName === userName);
        index < 0 &&
            this.users.push({
                userName: userName,
            });
    }

    removeUser(userName) {
        const index = _.findIndex(this.users, _user => _user.userName === userName);
        index >= 0 && this.users.splice(index, 1);
    }

    setFileContent(fileContent) {
        this.fileContent = fileContent;
    }

    otherUsers(userName) {
        return _.filter(this.users, c => c.userName !== userName);
    }

    hasOtherUsers(userName) {
        console.log(
            "has other users",
            this.otherUsers(userName).map(c => c.userName),
        );
        return this.otherUsers(userName).length > 0;
    }

    firstOtherUser(userName) {
        const otherUsers = this.otherUsers(userName);
        return otherUsers.length > 0 ? otherUsers[0].userName : "";
    }
}

class CodeManage {
    constructor() {
        this.inlineUsers = [];
        this.rooms = [];

        // eslint-disable-next-line no-unused-vars
        EvtBus.subscribe("User.Touch", user => {
            //console.log("UserTouch", user);
        });
    }

    addTouchUserInfo(userName, roomId) {
        const _user = this.inlineUsers.find(usr => usr.userName === userName);
        if (_user) {
            _user.roomId = roomId;
        } else {
            this.inlineUsers.push({
                userName: userName,
                roomId: roomId,
            });
        }
    }

    getTouchUserRoomId(userName) {
        const _user = this.inlineUsers.find(usr => usr.userName === userName);
        return _user ? _user.roomId : 0;
    }

    getRoom(roomId) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return null;
        } else {
            return this.rooms[index];
        }
    }

    findRoom(projectName, file, userName) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.projectName === projectName && _room.file === file;
        });

        if (index < 0) {
            return null;
        }

        const roomUsers = this.rooms[index].users;
        const has = _.some(roomUsers, _user => {
            return _user.userName === userName;
        });

        return has ? this.rooms[index] : null;
    }

    findRoomByProjectName(projectName) {
        return this.rooms.filter(_room => _room.projectName === projectName);
    }

    findRoomsByProjectName(projectName) {
        return this.rooms.filter(_room => _room.projectName === projectName);
    }

    inRoom(userName) {
        console.log(this.rooms);
        const index = _.findIndex(this.rooms, _room => {
            console.log(
                "room: ",
                _room.roomId,
                " users: ",
                _room.users.map(c => c.userName).join(","),
            );
            return _.some(_room.users, _user => {
                return _user.userName === userName;
            });
        });

        if (index < 0) {
            return null;
        }

        return this.rooms[index];
    }

    createRoom(projectName, file, creator) {
        // 用当前时间的毫秒数作为房间号，避免重复
        const roomId = this.makeRoomId();
        const newRoom = new Room(roomId);
        newRoom.projectName = projectName;
        newRoom.file = file;
        newRoom.creator = creator;
        newRoom.editor = creator;
        newRoom.addUser(creator);
        this.rooms.push(newRoom);
        // } else {
        // const index = _.findIndex(this.rooms, _room => {
        //     return _room.roomId === roomId;
        // });
        //     console.log("todo: ");
        //     const oldRoom = this.rooms[index]; //.addUser(user);
        //     oldRoom.projectName = projectName;
        //     oldRoom.file = file;
        //     oldRoom.creator = creator;
        // }

        return roomId;
    }

    // 根据projectName和file检索是否有room在编辑文件，如有返回roomId，否者返回0
    hasSameEditFile(projectName, file) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.projectName === projectName && _room.file === file;
        });

        return index >= 0 ? this.rooms[index].roomId : 0;
    }

    addRoomUser(roomId, user) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            const newRoom = new Room(roomId);
            newRoom.addUser(user);
            this.rooms.push(newRoom);
        } else {
            this.rooms[index].addUser(user);
        }
    }

    removeRoomUser(roomId, user) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index >= 0) {
            this.rooms[index].removeUser(user);
            if (this.rooms[index].users.length === 0) {
                this.rooms.splice(index, 1);
            }
        }
    }

    getRoomUsers(roomId) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return [];
        }

        return this.rooms[index].users.map(usr => {
            return { userName: usr.userName };
        });
    }

    hasOtherUsersInRoom(roomId, userName) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return false;
        }

        const roomUsers = this.rooms[index].users;
        const has = _.some(roomUsers, _user => {
            return _user.userName !== userName;
        });

        return has;
    }

    updateRoomInfo(roomId, projectName, file) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return;
        }

        const room = this.rooms[index];
        room.projectName = projectName;
        room.file = file;
    }

    setRoomFileContent(roomId, fileContent) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return;
        }

        this.rooms[index].fileContent = fileContent;
    }

    getRoomFileContent(roomId) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return "";
        }

        return this.rooms[index].fileContent;
    }

    setRoomEditor(roomId, editor) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return;
        }

        this.rooms[index].editor = editor;
    }

    removeRoom(roomId) {
        const index = _.findIndex(this.rooms, _room => {
            return _room.roomId === roomId;
        });

        if (index < 0) {
            return;
        }

        this.rooms.splice(index, 1);
    }

    makeRoomId() {
        return new Date().getMilliseconds();
    }
}

const cm = new CodeManage();

module.exports = cm;
//export default _cm;
