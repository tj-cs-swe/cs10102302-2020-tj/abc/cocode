## socketio接口
| 事件 | 描述 | emit方 | on方 | 传输数据 |
| :--: | :--: | :--: | :--: |:--: |
| connection | 连接 | 前端(自动触发) | 后端 | 无 |
| disconnect | 断开 | 前端(关闭界面自动触发) | 后端 | 无 |
| join | 加入房间 | 前端 | 后端 | {userName:xxx, projectName: xxx} |
| othersJoin | 他人加入 | 后端  | 前端 | userName |
| userList | 当前房间用户列表 | 后端 | 前端 | [userName1,userName2, …] |
| sendMessage | 发送消息 | 前端 | 后端 | {name: , avatar: , color: , message: ,mine: } |
| receiveMessage | 接收消息 | 后端 | 前端 | {name: , avatar: , color: , message: ,mine: } |
| othersLeave | 他人离开 | 后端 | 前端 | userName |

## 文件树方案
### 后端url
${this.$siteConfig.apiUrl()}/projectfile
### 主要功能
文件树需要实现新建文件(夹)、写入文件、删除文件(夹)、重命名、查看文件内容等几项重要功能
### 获取文件树流程
1. 前端发起post请求,获取项目文件树，req中包含项目名字（项目id可能更合适）
2. 后端利用fs模块获取该项目文件内容列表，返回给前端的扁平化数据格式如下(可根据前端需求修改)：

```javascript

    [

        {
            name:filename1,
            id:1,
            pid:0，
            type:dir
        },
        {
            name:filename2,
            id:2,
            pid:1，
            type:html
        },
        {
            name:filename3,
            id:3,
            pid:1，
            type:cpp
        }

    ]
```


3. 前端处理扁平化数据，使之成为嵌套树形数据，然后渲染出来

### 查看文件
1. 点击某个文件（非文件夹），前端发起get请求获取文件内容，req中包含文件路径
2. 后端返回给前端该路径下的文件内容
3. 前端界面显示文件内容

    post请求
    ```json
    req:{
    "operation":"GetFile",
    "projectName":"testProject",
    "path":"test/test.cpp"}

    res:{"status":"OK, content:文件内容"}
    ```

### 新建文件
1. 点击按钮后，前端发起post请求，req中包含文件路径等
2. 后端该路径中新建该文件，并返回状态(已经存在则返回ERROR)
3. 前端更新文件树

    post请求
    ```json
    req:{
    "operation":"NewFile",
    "projectName":"testProject",
    "path":"dir/2.txt",
    }

    res:{"status":"OK/ERROR"}
    ```

### 写入文件
1. 点击按钮后，前端发起post请求，req中包含文件路径等
2. 后端该路径中写入文件，并返回状态

    post请求
    ```json
    req:{
    "operation":"WriteFile",
    "projectName":"testProject",
    "path":"dir/2.txt",
    "content":""
    }

    res:{"status":"OK/ERROR"}
    ```

### 新建文件夹
1. 点击按钮后，前端发起post请求，req中包含文件路径等
2. 后端该路径中新建该文件夹，并返回状态
3. 前端更新文件树

    post请求
    ```json
    req:{
    "operation":"NewDir",
    "projectName":"testProject",
    "path":"dir",
    }

    res:{"status":"OK/ERROR"}
    ```

### 删除文件
1. 点击按钮后，前端发起post请求，req中包含文件路径等
2. 后端在该路径中删除该文件，并返回状态
3. 前端更新文件树

    post请求
    ```json
    req:{
    "operation":"DeleteFile",
    "projectName":"testProject",
    "path":"dir/1.txt"
    }

    res:{"status":"OK/ERROR"}
    ```

### 删除文件夹
1. 点击按钮后，前端发起post请求，req中包含文件路径等
2. 后端在该路径中删除该文件夹，并返回状态

    post请求
    ```json
    req:{
    "operation":"DelDir",
    "projectName":"testProject",
    "path":"dir1/dir2"
    }

    res:{"status":"OK/ERROR"}
    ```

### 重命名
1. 点击按钮后，前端发起post请求，req中包含文件路径，修改后的名字信息等
2. 后端将该路径下的文件(夹)修改名字，并返回状态

    post请求
    ```json
    req:{
    "operation":"RenameFile",
    "projectName":"testProject",
    "path":"dir1/dir2/name1.cpp",
    "rename":"name2.cpp"
    }

    res:{"status":"OK/ERROR"}
    ```


